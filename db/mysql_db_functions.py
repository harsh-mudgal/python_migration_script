from getpass import getpass
from mysql.connector import connect, Error
from migration_script_python.utils.credentials import *
from migration_script_python.utils.keywords import *


def connectToMySql():
    try:
        connection = connect(
            host="localhost",
            user=username,
            password=password,
            database=database,
        ) 
        if(connection.is_connected()):
            print("Connected")
            return connection
        else:
            raise Error()
    except Error as e:
        print("error: " + str(e))
        return -1
        
        
        
def checkForTable(cursor):
    checkQuery = "SHOW TABLES LIKE 'employee'"
    
    cursor.execute(checkQuery)
    result =cursor.fetchone()
    if result :
       print('table exists')
       return True
    else:
        print("no table exists")
        return False
    


def createEmployeeTable(cursor,mySqlConnection):
    createTableQuery = """
        CREATE TABLE employee(
            id INT AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(100),
            dob VARCHAR(10),
            department VARCHAR(100)
        )
        """
    cursor.execute(createTableQuery)
    mySqlConnection.commit()
    print("create table success")
    
    
    
def insertIntoEmployeeTable(records,cursor,mySqlConnection):
    insertQuery = """
        INSERT INTO employee
        ( name , dob , department )
        VALUES ( %s, %s , %s)
        """
    cursor.executemany(insertQuery , records)
    mySqlConnection.commit()




def fetchDataFromEmployee(cursor):
    selectQuery = "SELECT * FROM employee"
    cursor.execute(selectQuery)
    result = cursor.fetchall()
    print(result)
    return result
    