from migration_script_python.db.mysql_db_functions import *
from migration_script_python.model.employee_model import *
    
#connect to my sql db

def main(self):
    mySqlConnection=connectToMySql()
    cursor=mySqlConnection.cursor(buffered=True)

    results = fetchingRecordsFromMySQL(mySqlConnection=mySqlConnection , cursor=cursor)
    
    setRecordsInMongodb(results)

def fetchingRecordsFromMySQL(self,mySqlConnection , cursor) :
    
    # check weather table exist or not
    tableExists = checkForTable(cursor)
    print(tableExists)

    if(tableExists==False):
        print("table does not exist")
        print("enter number of records to enter")
        n = int(input())
        records=[]
        for i in range(0,n):
            record=input("Enter record : separated in name:department:dob format: ").split(':')
            records.append(convertEmployeeToTuple(Employee(i,record[0],record[1],record[2])))
            
        createEmployeeTable(cursor,mySqlConnection)
        insertIntoEmployeeTable(records,cursor,mySqlConnection)
        

    records=fetchDataFromEmployee(cursor)
    result=[]
    for i in records:
        result.append(convertTupleToEmloyee(i))
    return result
    
def setRecordsInMongodb(results):
    pass

if __name__ == '__main__': 
    main()


# ("harsh","17/10/2001","IT"), ("Kop" , "20/08/2002" , "HR")
# harsh:It:17/10/2001
# ko:Hr:20/08/2002

